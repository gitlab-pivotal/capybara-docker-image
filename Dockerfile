FROM ruby:2.1

# Requirements for capybara-webkit
RUN apt-get update \
  && apt-get install -y qt5-default libqt5webkit5-dev g++ zip xvfb s3cmd

CMD [ "/bin/bash" ]